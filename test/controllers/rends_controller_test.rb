require 'test_helper'

class RendsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rend = rends(:one)
  end

  test "should get index" do
    get rends_url, as: :json
    assert_response :success
  end

  test "should create rend" do
    assert_difference('Rend.count') do
      post rends_url, params: { rend: { car_id: @rend.car_id, end: @rend.end, start: @rend.start } }, as: :json
    end

    assert_response 201
  end

  test "should show rend" do
    get rend_url(@rend), as: :json
    assert_response :success
  end

  test "should update rend" do
    patch rend_url(@rend), params: { rend: { car_id: @rend.car_id, end: @rend.end, start: @rend.start } }, as: :json
    assert_response 200
  end

  test "should destroy rend" do
    assert_difference('Rend.count', -1) do
      delete rend_url(@rend), as: :json
    end

    assert_response 204
  end
end
