class Rend < ApplicationRecord
  belongs_to :car


  def count_price_rend
    if (final - start).to_i < 30
      car.day_price = car.day_price * (final - start).to_i
    end
    if (final - start).to_i >= 30 && (final - start).to_i < 360
      car.month_price = car.month_price * ((final - start).to_i/30)
    end
    if (final - start).to_i >=360
      car.year_price = car.year_price * ((final - start).to_i/360)
    end
  end


end
