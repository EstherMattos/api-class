class CreateCars < ActiveRecord::Migration[5.2]
  def change
    create_table :cars do |t|
      t.string :model
      t.references :manufacturer, foreign_key: true
      t.text :specs
      t.integer :day_price
      t.integer :month_price
      t.integer :year_price
      t.integer :sale_price
      t.integer :quant_cars

      t.timestamps
    end
  end
end
