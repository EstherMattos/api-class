class CreateRends < ActiveRecord::Migration[5.2]
  def change
    create_table :rends do |t|
      t.references :car, foreign_key: true
      t.integer :start
      t.integer :end

      t.timestamps
    end
  end
end
