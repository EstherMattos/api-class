class ChangeColumnEnd < ActiveRecord::Migration[5.2]
  def change
    rename_column :rends, :end, :final
  end
end
