class ChangeTypeData < ActiveRecord::Migration[5.2]
  def change
    change_column :rends, :start, :date
    change_column :rends, :final, :date
  end
end
